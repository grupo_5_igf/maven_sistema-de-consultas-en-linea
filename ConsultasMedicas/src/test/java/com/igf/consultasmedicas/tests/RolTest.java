/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.tests;

import com.igf.consultasmedicas.entity.Rol;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author mata
 */
public class RolTest {
    
     private static EntityManager manager;
     
     private static EntityManagerFactory emf;
     
     public static void main(String[] args){
         emf=Persistence.createEntityManagerFactory("aplicacion");
         manager=emf.createEntityManager();
         List<Rol>roles=manager.createQuery("FROM Rol").getResultList();
         System.out.print("Hay actualmante "+roles.size()+" Roles: ");
         for(Rol rol: roles){
             System.out.print(rol.toString());
         }
         System.exit(0);
     }
}
