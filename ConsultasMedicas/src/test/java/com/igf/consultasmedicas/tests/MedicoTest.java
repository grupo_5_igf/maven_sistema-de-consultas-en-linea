/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.tests;

import com.igf.consultasmedicas.entity.Medico;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author mata
 */
public class MedicoTest {
      
     private static EntityManager manager;
     
     private static EntityManagerFactory emf;
     
     public static void main(String[] args){
         emf=Persistence.createEntityManagerFactory("aplicacion");
         manager=emf.createEntityManager();
         List<Medico>medicos=manager.createQuery("SELECT m FROM Medico m").getResultList();
         System.out.print("Hay actualmante "+medicos.size()+" Medicos: ");
         for(Medico medico: medicos){
             System.out.print(medico.toString());
         }
         System.exit(0);
     }
}
