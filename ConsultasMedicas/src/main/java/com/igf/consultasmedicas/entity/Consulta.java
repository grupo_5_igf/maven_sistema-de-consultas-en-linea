/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mata
 */
@Entity
@Table( name="consulta" ,uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id_cita"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consulta.findAll", query = "SELECT c FROM Consulta c")
    , @NamedQuery(name = "Consulta.findByIdCita", query = "SELECT c FROM Consulta c WHERE c.idCita = :idCita")
    , @NamedQuery(name = "Consulta.findByComentarios", query = "SELECT c FROM Consulta c WHERE c.comentarios = :comentarios")})
public class Consulta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_cita", nullable = false)
    private Integer idCita;
    @Size(max = 100)
    @Column(length = 100)
    private String comentarios;
    @JoinTable(name = "relationship_5", joinColumns = {
        @JoinColumn(name = "id_cita", referencedColumnName = "id_cita", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "id_medico", referencedColumnName = "id_medico", nullable = false)})
    @ManyToMany
    private Set<Medico> medicoSet;
    @JoinColumn(name = "id_persona", referencedColumnName = "id_persona")
    @ManyToOne
    private Paciente idPersona;

    public Consulta() {
    }

    public Consulta(Integer idCita) {
        this.idCita = idCita;
    }

    public Integer getIdCita() {
        return idCita;
    }

    public void setIdCita(Integer idCita) {
        this.idCita = idCita;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    @XmlTransient
    public Set<Medico> getMedicoSet() {
        return medicoSet;
    }

    public void setMedicoSet(Set<Medico> medicoSet) {
        this.medicoSet = medicoSet;
    }

    public Paciente getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Paciente idPersona) {
        this.idPersona = idPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCita != null ? idCita.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consulta)) {
            return false;
        }
        Consulta other = (Consulta) object;
        if ((this.idCita == null && other.idCita != null) || (this.idCita != null && !this.idCita.equals(other.idCita))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.igf.consultasmedicas.entity.Consulta[ idCita=" + idCita + " ]";
    }
    
}
