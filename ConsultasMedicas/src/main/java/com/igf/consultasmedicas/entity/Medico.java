/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mata
 */
@Entity
@Table(name = "medico" , uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id_medico"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Medico.findAll", query = "SELECT m FROM Medico m")
    , @NamedQuery(name = "Medico.findByIdMedico", query = "SELECT m FROM Medico m WHERE m.idMedico = :idMedico")
    , @NamedQuery(name = "Medico.findByNombreMedico", query = "SELECT m FROM Medico m WHERE m.nombreMedico = :nombreMedico")
    , @NamedQuery(name = "Medico.findByApellidoMedico", query = "SELECT m FROM Medico m WHERE m.apellidoMedico = :apellidoMedico")
    , @NamedQuery(name = "Medico.findByTelefonoMedico", query = "SELECT m FROM Medico m WHERE m.telefonoMedico = :telefonoMedico")
    , @NamedQuery(name = "Medico.findByDireccionMedico", query = "SELECT m FROM Medico m WHERE m.direccionMedico = :direccionMedico")
    , @NamedQuery(name = "Medico.findByCorreoMedico", query = "SELECT m FROM Medico m WHERE m.correoMedico = :correoMedico")
    , @NamedQuery(name = "Medico.findByCodigoMedico", query = "SELECT m FROM Medico m WHERE m.codigoMedico = :codigoMedico")
    , @NamedQuery(name = "Medico.findByFotoMedico", query = "SELECT m FROM Medico m WHERE m.fotoMedico = :fotoMedico")
    , @NamedQuery(name = "Medico.findByEstadoMedico", query = "SELECT m FROM Medico m WHERE m.estadoMedico = :estadoMedico")
    , @NamedQuery(name = "Medico.findBySexo", query = "SELECT m FROM Medico m WHERE m.sexo = :sexo")
    , @NamedQuery(name = "Medico.findByEstadoCivil", query = "SELECT m FROM Medico m WHERE m.estadoCivil = :estadoCivil")})
public class Medico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_medico", nullable = false)
    private Integer idMedico;
    @Size(max = 30)
    @Column(name = "nombre_Medico", length = 30)
    private String nombreMedico;
    @Size(max = 30)
    @Column(name = "apellido_Medico", length = 30)
    private String apellidoMedico;
    @Size(max = 9)
    @Column(name = "telefono_Medico", length = 9)
    private String telefonoMedico;
    @Size(max = 20)
    @Column(name = "direccion_Medico", length = 20)
    private String direccionMedico;
    @Size(max = 25)
    @Column(name = "correo_Medico", length = 25)
    private String correoMedico;
    @Size(max = 4)
    @Column(name = "codigo_medico", length = 4)
    private String codigoMedico;
    @Size(max = 100)
    @Column(name = "foto_Medico", length = 100)
    private String fotoMedico;
    @Column(name = "estado_medico")
    private Boolean estadoMedico;
    private Character sexo;
    @Size(max = 10)
    @Column(name = "estado_civil", length = 10)
    private String estadoCivil;
    @ManyToMany(mappedBy = "medicoSet")
    private Set<Consulta> consultaSet;
    @JoinColumn(name = "id_especialidad", referencedColumnName = "id_especialidad", insertable = false, updatable = false)
    @ManyToOne
    private Especialidad idEspecialidad;
    
    @Column(name="id_especialidad")
    private Integer especialidad;

    public Integer getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(Integer especialidad) {
        this.especialidad = especialidad;
    }

    @JoinColumn(name = "id_institucion", referencedColumnName = "id_institucion", insertable = false, updatable = false)
    @ManyToOne
    private Institucion idInstitucion;
    
    @Column(name="id_institucion")
    private Integer institucion;
    
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(cascade=CascadeType.ALL)
    private Usuario idUsuario;
    
    @Column(name="id_usuario", insertable = false, updatable = false )
    private Integer usuarios;


    public Medico() {
    }

    public Medico(Integer idMedico) {
        this.idMedico = idMedico;
    }

    public Integer getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(Integer idMedico) {
        this.idMedico = idMedico;
    }

    public String getNombreMedico() {
        return nombreMedico;
    }

    public void setNombreMedico(String nombreMedico) {
        this.nombreMedico = nombreMedico;
    }

    public String getApellidoMedico() {
        return apellidoMedico;
    }

    public void setApellidoMedico(String apellidoMedico) {
        this.apellidoMedico = apellidoMedico;
    }

    public String getTelefonoMedico() {
        return telefonoMedico;
    }

    public void setTelefonoMedico(String telefonoMedico) {
        this.telefonoMedico = telefonoMedico;
    }

    public String getDireccionMedico() {
        return direccionMedico;
    }

    public void setDireccionMedico(String direccionMedico) {
        this.direccionMedico = direccionMedico;
    }

    public String getCorreoMedico() {
        return correoMedico;
    }

    public void setCorreoMedico(String correoMedico) {
        this.correoMedico = correoMedico;
    }

    public String getCodigoMedico() {
        return codigoMedico;
    }

    public void setCodigoMedico(String codigoMedico) {
        this.codigoMedico = codigoMedico;
    }

    public String getFotoMedico() {
        return fotoMedico;
    }

    public void setFotoMedico(String fotoMedico) {
        this.fotoMedico = fotoMedico;
    }

    public Boolean getEstadoMedico() {
        return estadoMedico;
    }

    public void setEstadoMedico(Boolean estadoMedico) {
        this.estadoMedico = estadoMedico;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }
    

    @XmlTransient
    public Set<Consulta> getConsultaSet() {
        return consultaSet;
    }

    public void setConsultaSet(Set<Consulta> consultaSet) {
        this.consultaSet = consultaSet;
    }

    public Especialidad getIdEspecialidad() {
        return idEspecialidad;
    }

    public void setIdEspecialidad(Especialidad idEspecialidad) {
        this.idEspecialidad = idEspecialidad;
    }

    public Institucion getIdInstitucion() {
        return idInstitucion;
    }
    
    public Integer getInstitucion() {
        return institucion;
    }

    public void setInstitucion(Integer institucion) {
        this.institucion = institucion;
    }

    public void setIdInstitucion(Institucion idInstitucion) {
        this.idInstitucion = idInstitucion;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    
    public Integer getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Integer usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMedico != null ? idMedico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Medico)) {
            return false;
        }
        Medico other = (Medico) object;
        if ((this.idMedico == null && other.idMedico != null) || (this.idMedico != null && !this.idMedico.equals(other.idMedico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.igf.consultasmedicas.entity.Medico[ idMedico=" + idMedico + " ]";
    }
    
}
