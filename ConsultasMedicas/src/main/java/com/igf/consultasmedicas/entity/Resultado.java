/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mata
 */
@Entity
@Table(name="resultado", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id_resultado"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Resultado.findAll", query = "SELECT r FROM Resultado r")
    , @NamedQuery(name = "Resultado.findByIdResultado", query = "SELECT r FROM Resultado r WHERE r.idResultado = :idResultado")
    , @NamedQuery(name = "Resultado.findByFechaPrescipcion", query = "SELECT r FROM Resultado r WHERE r.fechaPrescipcion = :fechaPrescipcion")
    , @NamedQuery(name = "Resultado.findByFechaRealizacion", query = "SELECT r FROM Resultado r WHERE r.fechaRealizacion = :fechaRealizacion")
    , @NamedQuery(name = "Resultado.findByLectura", query = "SELECT r FROM Resultado r WHERE r.lectura = :lectura")})
public class Resultado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_resultado", nullable = false)
    private Integer idResultado;
    
    @Column(name = "fecha_prescipcion")
    @Temporal(TemporalType.DATE)
    private Date fechaPrescipcion;
    
    @Column(name = "fecha_realizacion")
    @Temporal(TemporalType.DATE)
    private Date fechaRealizacion;
    
    @Size(max = 200)
    @Column(length = 200)
    private String lectura;
    
    @JoinColumn(name = "id_examen", referencedColumnName = "id_examen")
    @ManyToOne(cascade=CascadeType.ALL)
    private Examen idExamen;
    
    @JoinColumn(name = "id_tratamiento", referencedColumnName = "id_tratamiento")
    @ManyToOne(fetch=FetchType.LAZY)
    private Tratamiento idTratamiento;

    public Resultado() {
    }

    public Resultado(Integer idResultado) {
        this.idResultado = idResultado;
    }

    public Integer getIdResultado() {
        return idResultado;
    }

    public void setIdResultado(Integer idResultado) {
        this.idResultado = idResultado;
    }

    public Date getFechaPrescipcion() {
        return fechaPrescipcion;
    }

    public void setFechaPrescipcion(Date fechaPrescipcion) {
        this.fechaPrescipcion = fechaPrescipcion;
    }

    public Date getFechaRealizacion() {
        return fechaRealizacion;
    }

    public void setFechaRealizacion(Date fechaRealizacion) {
        this.fechaRealizacion = fechaRealizacion;
    }

    public String getLectura() {
        return lectura;
    }

    public void setLectura(String lectura) {
        this.lectura = lectura;
    }

    public Examen getIdExamen() {
        return idExamen;
    }

    public void setIdExamen(Examen idExamen) {
        this.idExamen = idExamen;
    }

    
    public Tratamiento getIdTratamiento() {
        return idTratamiento;
    }

    public void setIdTratamiento(Tratamiento idTratamiento) {
        this.idTratamiento = idTratamiento;
    }

   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idResultado != null ? idResultado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resultado)) {
            return false;
        }
        Resultado other = (Resultado) object;
        if ((this.idResultado == null && other.idResultado != null) || (this.idResultado != null && !this.idResultado.equals(other.idResultado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.igf.consultasmedicas.entity.Resultado[ idResultado=" + idResultado + " ]";
    }
    
}
