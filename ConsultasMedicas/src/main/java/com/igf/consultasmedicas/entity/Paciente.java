/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mata
 */
@Entity
@Table(name="paciente", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id_Persona"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Paciente.findAll", query = "SELECT p FROM Paciente p")
    , @NamedQuery(name = "Paciente.findByIdPersona", query = "SELECT p FROM Paciente p WHERE p.idPersona= :idPersona")
    , @NamedQuery(name = "Paciente.findByNombrePaciente", query = "SELECT p FROM Paciente p WHERE p.nombrePaciente = :nombrePaciente")
    , @NamedQuery(name = "Paciente.findByApellidoPaciente", query = "SELECT p FROM Paciente p WHERE p.apellidoPaciente = :apellidoPaciente")
    , @NamedQuery(name = "Paciente.findByTelefonoPaciente", query = "SELECT p FROM Paciente p WHERE p.telefonoPaciente = :telefonoPaciente")
    , @NamedQuery(name = "Paciente.findByDireccionPaciente", query = "SELECT p FROM Paciente p WHERE p.direccionPaciente = :direccionPaciente")
    , @NamedQuery(name = "Paciente.findByCorreoPaciente", query = "SELECT p FROM Paciente p WHERE p.correoPaciente = :correoPaciente")
    , @NamedQuery(name = "Paciente.findByFotoPaciente", query = "SELECT p FROM Paciente p WHERE p.fotoPaciente = :fotoPaciente")
    , @NamedQuery(name = "Paciente.findByEstadoPaciente", query = "SELECT p FROM Paciente p WHERE p.estadoPaciente= :estadoPaciente")
    , @NamedQuery(name = "Paciente.findBySexo", query = "SELECT p FROM Paciente p WHERE p.sexo = :sexo")
    , @NamedQuery(name = "Paciente.findByEstadoCivil", query = "SELECT p FROM Paciente p WHERE p.estadoCivil = :estadoCivil")})
public class Paciente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Persona", nullable = false)
    private Integer idPersona;
    @Size(max = 30)
    @Column(name = "nombre_paciente", length = 30)
    private String nombrePaciente;
    @Size(max = 30)
    @Column(name = "apellido_paciente", length = 30)
    private String apellidoPaciente;
    @Size(max = 9)
    @Column(name = "telefono_paciente", length = 9)
    private String telefonoPaciente;
    @Size(max = 20)
    @Column(name = "direccion_paciente", length = 20)
    private String direccionPaciente;
    @Size(max = 25)
    @Column(name = "correo_paciente", length = 25)
    private String correoPaciente;
    @Size(max = 100)
    @Column(name = "foto_paciente", length = 100)
    private String fotoPaciente;
    @Column(name = "estado_paciente")
    private Boolean estadoPaciente;
    private Character sexo;
    @Size(max = 10)
    @Column(name = "estado_civil", length = 10)
    private String estadoCivil;
    @OneToMany(mappedBy = "idPersona")
    private Set<Expediente> expedienteSet;
    @JoinColumn(name = "id_expediente", referencedColumnName = "id_expediente")
    @ManyToOne(cascade=CascadeType.ALL)
    private Expediente idExpediente;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario", insertable = false, updatable = false)
    @ManyToOne
    private Usuario idUsuario;
    @Column(name="id_usuario")
    Integer usuario;

    @OneToMany(mappedBy = "idPersona")
    private Set<Consulta> consultaSet;

    public Paciente() {
    }

    public Paciente(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public void setNombrePaciente(String nombrePaciente) {
        this.nombrePaciente = nombrePaciente;
    }

    public String getApellidoPaciente() {
        return apellidoPaciente;
    }

    public void setApellidoPaciente(String apellidoPaciente) {
        this.apellidoPaciente = apellidoPaciente;
    }

    public String getTelefonoPaciente() {
        return telefonoPaciente;
    }

    public void setTelefonoPaciente(String telefonoPaciente) {
        this.telefonoPaciente = telefonoPaciente;
    }

    public String getDireccionPaciente() {
        return direccionPaciente;
    }

    public void setDireccionPaciente(String direccionPaciente) {
        this.direccionPaciente = direccionPaciente;
    }

    public String getCorreoPaciente() {
        return correoPaciente;
    }

    public void setCorreoPaciente(String correoPaciente) {
        this.correoPaciente = correoPaciente;
    }

    public String getFotoPaciente() {
        return fotoPaciente;
    }

    public void setFotoPaciente(String fotoPaciente) {
        this.fotoPaciente = fotoPaciente;
    }

    public Boolean getEstadoPaciente() {
        return estadoPaciente;
    }

    public void setEstadoPaciente(Boolean estadoPaciente) {
        this.estadoPaciente = estadoPaciente;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    @XmlTransient
    public Set<Expediente> getExpedienteSet() {
        return expedienteSet;
    }

    public void setExpedienteSet(Set<Expediente> expedienteSet) {
        this.expedienteSet = expedienteSet;
    }

    public Expediente getIdExpediente() {
        return idExpediente;
    }

    public void setIdExpediente(Expediente idExpediente) {
        this.idExpediente = idExpediente;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }

    @XmlTransient
    public Set<Consulta> getConsultaSet() {
        return consultaSet;
    }

    public void setConsultaSet(Set<Consulta> consultaSet) {
        this.consultaSet = consultaSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPersona != null ? idPersona.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paciente)) {
            return false;
        }
        Paciente other = (Paciente) object;
        if ((this.idPersona == null && other.idPersona != null) || (this.idPersona != null && !this.idPersona.equals(other.idPersona))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.igf.consultasmedicas.entity.Paciente[ idPersona=" + idPersona + " ]";
    }
    
}
