/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mata
 */
@Entity
@Table(name="dosis", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id_dosis"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dosis.findAll", query = "SELECT d FROM Dosis d")
    , @NamedQuery(name = "Dosis.findByIdDosis", query = "SELECT d FROM Dosis d WHERE d.idDosis = :idDosis")
    , @NamedQuery(name = "Dosis.findByCantidad", query = "SELECT d FROM Dosis d WHERE d.cantidad = :cantidad")
    , @NamedQuery(name = "Dosis.findByDosis", query = "SELECT d FROM Dosis d WHERE d.dosis = :dosis")})
public class Dosis implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_dosis", nullable = false)
    private Integer idDosis;
    private Integer cantidad;
    @Size(max = 100)
    @Column(length = 100)
    private String dosis;
    @JoinColumn(name = "id_medicamento", referencedColumnName = "id_medicamento")
    @ManyToOne
    private Medicamento idMedicamento;
    @JoinColumn(name = "id_tratamiento", referencedColumnName = "id_tratamiento")
    @ManyToOne
    private Tratamiento idTratamiento;

    public Dosis() {
    }

    public Dosis(Integer idDosis) {
        this.idDosis = idDosis;
    }

    public Integer getIdDosis() {
        return idDosis;
    }

    public void setIdDosis(Integer idDosis) {
        this.idDosis = idDosis;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public Medicamento getIdMedicamento() {
        return idMedicamento;
    }

    public void setIdMedicamento(Medicamento idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    public Tratamiento getIdTratamiento() {
        return idTratamiento;
    }

    public void setIdTratamiento(Tratamiento idTratamiento) {
        this.idTratamiento = idTratamiento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDosis != null ? idDosis.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dosis)) {
            return false;
        }
        Dosis other = (Dosis) object;
        if ((this.idDosis == null && other.idDosis != null) || (this.idDosis != null && !this.idDosis.equals(other.idDosis))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.igf.consultasmedicas.entity.Dosis[ idDosis=" + idDosis + " ]";
    }
    
}
