/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mata
 */
@Entity
@Table(name="especialidad", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id_especialidad"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Especialidad.findAll", query = "SELECT e FROM Especialidad e")
    , @NamedQuery(name = "Especialidad.findByIdEspecialidad", query = "SELECT e FROM Especialidad e WHERE e.idEspecialidad = :idEspecialidad")
    , @NamedQuery(name = "Especialidad.findByNombreEspecialidad", query = "SELECT e FROM Especialidad e WHERE e.nombreEspecialidad = :nombreEspecialidad")
    , @NamedQuery(name = "Especialidad.findByEstadoEspecialidad", query = "SELECT e FROM Especialidad e WHERE e.estadoEspecialidad = :estadoEspecialidad")})
public class Especialidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_especialidad", nullable = false)
    private Integer idEspecialidad;
    @Size(max = 25)
    @Column(name = "nombre_especialidad", length = 25)
    private String nombreEspecialidad;
    @Column(name = "estado_especialidad")
    private Boolean estadoEspecialidad;
    @OneToMany(mappedBy = "idEspecialidad")
    private Set<Medico> medicoSet;

    public Especialidad() {
    }

    public Especialidad(Integer idEspecialidad) {
        this.idEspecialidad = idEspecialidad;
    }

    public Integer getIdEspecialidad() {
        return idEspecialidad;
    }

    public void setIdEspecialidad(Integer idEspecialidad) {
        this.idEspecialidad = idEspecialidad;
    }

    public String getNombreEspecialidad() {
        return nombreEspecialidad;
    }

    public void setNombreEspecialidad(String nombreEspecialidad) {
        this.nombreEspecialidad = nombreEspecialidad;
    }

    public Boolean getEstadoEspecialidad() {
        return estadoEspecialidad;
    }

    public void setEstadoEspecialidad(Boolean estadoEspecialidad) {
        this.estadoEspecialidad = estadoEspecialidad;
    }

    @XmlTransient
    public Set<Medico> getMedicoSet() {
        return medicoSet;
    }

    public void setMedicoSet(Set<Medico> medicoSet) {
        this.medicoSet = medicoSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEspecialidad != null ? idEspecialidad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
    if(this.getIdEspecialidad() == ((Especialidad) obj).getIdEspecialidad()) {
        return true;
    }else {
        return false;
    }
}

    @Override
    public String toString() {
        return "com.igf.consultasmedicas.entity.Especialidad[ idEspecialidad=" + idEspecialidad + " ]";
    }
    
}
