/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mata
 */
@Entity
@Table(name="expediente", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id_expediente"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Expediente.findAll", query = "SELECT e FROM Expediente e")
    , @NamedQuery(name = "Expediente.findByIdExpediente", query = "SELECT e FROM Expediente e WHERE e.idExpediente = :idExpediente")
    , @NamedQuery(name = "Expediente.findByNumeroExpediente", query = "SELECT e FROM Expediente e WHERE e.numeroExpediente = :numeroExpediente")
    , @NamedQuery(name = "Expediente.findByFechaApertura", query = "SELECT e FROM Expediente e WHERE e.fechaApertura = :fechaApertura")})
public class Expediente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_expediente", nullable = false)
    private Integer idExpediente;
    
    @Size(max = 5)
    @Column(name = "numero_expediente", length = 5)
    private String numeroExpediente;
   
    @Column(name = "fecha_apertura")
    @Temporal(TemporalType.DATE)
    private Date fechaApertura;
   
    
    @JoinColumn(name = "id_persona", referencedColumnName = "id_persona")
    @ManyToOne
    private Paciente idPersona;
    
    @OneToMany(mappedBy = "idExpediente")
    private Set<Tratamiento> tratamientoSet;
    
    @OneToMany(mappedBy = "idExpediente")
    private Set<Paciente> pacienteSet;
    
   

    public Expediente() {
    }

    public Expediente(Integer idExpediente) {
        this.idExpediente = idExpediente;
    }

    public Integer getIdExpediente() {
        return idExpediente;
    }

    public void setIdExpediente(Integer idExpediente) {
        this.idExpediente = idExpediente;
    }

    public String getNumeroExpediente() {
        return numeroExpediente;
    }

    public void setNumeroExpediente(String numeroExpediente) {
        this.numeroExpediente = numeroExpediente;
    }

    public Date getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(Date fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public Paciente getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Paciente idPersona) {
        this.idPersona = idPersona;
    }

    @XmlTransient
    public Set<Tratamiento> getTratamientoSet() {
        return tratamientoSet;
    }

    public void setTratamientoSet(Set<Tratamiento> tratamientoSet) {
        this.tratamientoSet = tratamientoSet;
    }

    @XmlTransient
    public Set<Paciente> getPacienteSet() {
        return pacienteSet;
    }

    public void setPacienteSet(Set<Paciente> pacienteSet) {
        this.pacienteSet = pacienteSet;
    }

   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idExpediente != null ? idExpediente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Expediente)) {
            return false;
        }
        Expediente other = (Expediente) object;
        if ((this.idExpediente == null && other.idExpediente != null) || (this.idExpediente != null && !this.idExpediente.equals(other.idExpediente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.igf.consultasmedicas.entity.Expediente[ idExpediente=" + idExpediente + " ]";
    }
    
}
