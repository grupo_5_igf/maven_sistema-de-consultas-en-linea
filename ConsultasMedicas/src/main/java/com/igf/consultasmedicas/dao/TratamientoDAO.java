/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.dao;

import com.igf.consultasmedicas.entity.Tratamiento;
import javax.persistence.EntityManager;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author mata
 */
public class TratamientoDAO {
     EntityManagerFactory emf;
     EntityManager manager;
     
     public TratamientoDAO() {
           emf = Persistence.createEntityManagerFactory("aplicacion");
           manager=emf.createEntityManager();
    }
     
      public Tratamiento agregarTratamiento(Tratamiento tratamiento)  {
         emf = Persistence.createEntityManagerFactory("aplicacion");
        manager=emf.createEntityManager();
        manager.getTransaction().begin();
        try {
       manager.persist(tratamiento);
       manager.getTransaction().commit();
       manager.refresh(tratamiento);
        } catch (Exception ex) {
            
            System.out.print("-----------------Excepcion----"+ex);
        }
        
         manager.clear();
        manager.close();
       return tratamiento;
    }
      //  Lista los tratamientos    
    public List<Tratamiento> listarTratamientos(){
        emf = Persistence.createEntityManagerFactory("aplicacion");
        manager=emf.createEntityManager();
        Query query = manager.createQuery("SELECT m FROM Tratamiento");
        List<Tratamiento>medicos=(List<Tratamiento>)query.getResultList();
       
        System.out.print("Hay actualmante --"+medicos.size()+"--------------- Tratamientos: -------------");
        manager.clear();
        manager.close();
         return medicos;
    }
    //    Obtener tratamiento por id
    public Tratamiento obtenerTratamiento(Integer id){
         manager=emf.createEntityManager();
         Tratamiento tratamiento=new Tratamiento();
         tratamiento=manager.find(Tratamiento.class, id);
         manager.refresh(tratamiento);
         manager.clear();
         manager.close();
         return tratamiento;
    }
   
    
}
