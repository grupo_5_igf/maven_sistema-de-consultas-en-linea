/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.dao;

import com.igf.consultasmedicas.entity.Expediente;
import com.igf.consultasmedicas.entity.Paciente;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author mata
 */
public class ExpedienteDAO {
    EntityManagerFactory emf;
     EntityManager manager;
     
     public ExpedienteDAO() {
           emf = Persistence.createEntityManagerFactory("aplicacion");
           manager=emf.createEntityManager();
    }
     //  Lista los medicos    
    public List<Paciente> listarPacientes(){
        emf = Persistence.createEntityManagerFactory("aplicacion");
        manager=emf.createEntityManager();
        Query query = manager.createQuery("SELECT p FROM Paciente p where p.estadoPaciente='true'");
        List<Paciente>pacientes=(List<Paciente>)query.getResultList();
       
        System.out.print("Hay actualmante --"+pacientes.size()+"--------------- Pacientes Activos: -------------");
        manager.clear();
        manager.close();
         return pacientes;
    }
//  Obtener el paciente de acuerdo al id    
    public Paciente obtenerPaciente(Integer id){
         manager=emf.createEntityManager();
         Paciente paciente=new Paciente();
         paciente=manager.find(Paciente.class, id);
         manager.clear();
         manager.close();
         return paciente;
    }
 // Obtener Expediente
     public Expediente obtenerExpediente(Integer id){
         manager=emf.createEntityManager();
         Expediente expediente=new Expediente();
         expediente=manager.find(Expediente.class, id);
         manager.clear();
         manager.close();
         return expediente;
    }
//  Actualizar Paciente
    public void actualizarPaciente(Paciente paciente){
        manager=emf.createEntityManager();
        manager.getTransaction().begin();
         try{
            manager.merge(paciente);
            manager.getTransaction().commit();
            manager.clear();
            manager.close();
    }  catch (Exception ex) {
         System.out.print("-----------------Excepcion----"+ex);
        }
    }
}
