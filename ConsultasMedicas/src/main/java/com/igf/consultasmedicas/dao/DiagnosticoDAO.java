/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.dao;

import com.igf.consultasmedicas.entity.Diagnosticos;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author mata
 */
public class DiagnosticoDAO {
     EntityManagerFactory emf;
     EntityManager manager;
     
     public DiagnosticoDAO() {
           emf = Persistence.createEntityManagerFactory("aplicacion");
           manager=emf.createEntityManager();
    }
     
     //  Guardar diagnostico y su respectivo usuario    
    public Diagnosticos agregarDiagnostico(Diagnosticos diagnostico)  {
       emf = Persistence.createEntityManagerFactory("aplicacion");
       manager=emf.createEntityManager();
       manager.getTransaction().begin();
       try {
       manager.persist(diagnostico);
       manager.getTransaction().commit();
       manager.refresh(diagnostico);
        } catch (Exception ex) {
            
            System.out.print("-----------------Excepcion----"+ex);
        }
       
       manager.clear();
        manager.close();
       return diagnostico;
}
}
