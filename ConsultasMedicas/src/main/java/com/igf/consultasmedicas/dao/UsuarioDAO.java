/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.dao;

import com.igf.consultasmedicas.entity.Usuario;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author mata
 */
public class UsuarioDAO {
     EntityManagerFactory emf;
     EntityManager manager;
    
     public UsuarioDAO() {
           emf = Persistence.createEntityManagerFactory("aplicacion");
           manager=emf.createEntityManager();
    }
//     
//     Guardar un usuario nuevo
     public Usuario agregarUsuario(Usuario usuario)  {
       emf = Persistence.createEntityManagerFactory("aplicacion");
       manager=emf.createEntityManager();
       manager.getTransaction().begin();
       try {
       manager.persist(usuario);
       manager.getTransaction().commit();
       manager.refresh(usuario);
        } catch (Exception ex) {
            
            System.out.print("-----------------Excepcion----"+ex);
        }
       manager.clear();
       manager.close();
       return usuario;
}
     
     public Boolean verificarNombre(String nombre){
        emf = Persistence.createEntityManagerFactory("aplicacion");
        manager=emf.createEntityManager();
        
        Query query=manager.createQuery("SELECT u FROM Usuario u where u.nombreUsuario=:nombre").setParameter("nombre", nombre);
        List<Usuario> usuarios=(List<Usuario>)query.getResultList();
        manager.clear();
        manager.close();
        if(!usuarios.isEmpty()){
           return false; 
        }else{
            return true;
        } 
     }
     public Usuario obtenerUsuario(String nombre){
        emf = Persistence.createEntityManagerFactory("aplicacion");
        manager=emf.createEntityManager();
        Usuario usuario=new Usuario();
        try {
        Query query=manager.createQuery("SELECT u FROM Usuario u where u.nombreUsuario=:nombre and u.estadoUsuario='true'").setParameter("nombre", nombre);
        List<Usuario> usuarios=(List<Usuario>)query.getResultList();
        usuario=usuarios.get(0);
        manager.refresh(usuario);
        manager.clear();
        manager.close();
        emf.close();
        return usuario;
         } catch (Exception ex) {
             System.out.print("-----------------Excepcion----"+ex);
             manager.close();
             return usuario;
         }
        
     }
}
