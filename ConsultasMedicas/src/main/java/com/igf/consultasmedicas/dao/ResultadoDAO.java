/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.dao;

import com.igf.consultasmedicas.entity.Resultado;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author mata
 */
public class ResultadoDAO {
     EntityManagerFactory emf;
     EntityManager manager;
     
     public ResultadoDAO() {
           emf = Persistence.createEntityManagerFactory("aplicacion");
           manager=emf.createEntityManager();
    }
     
     //  Guardar resultado y su respectivo usuario    
    public Resultado agregarDiagnostico(Resultado resultado)  {
       emf = Persistence.createEntityManagerFactory("aplicacion");
       manager=emf.createEntityManager();
       manager.getTransaction().begin();
       try {
       manager.persist(resultado);
       manager.getTransaction().commit();
       manager.refresh(resultado);
        } catch (Exception ex) {
            
            System.out.print("-----------------Excepcion----"+ex);
        }
       
       manager.clear();
        manager.close();
       return resultado;
}
}
