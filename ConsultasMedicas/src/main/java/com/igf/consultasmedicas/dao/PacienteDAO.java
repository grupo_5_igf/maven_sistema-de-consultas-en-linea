/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.dao;

import com.igf.consultasmedicas.entity.Paciente;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author mata
 */
public class PacienteDAO {
    EntityManagerFactory emf;
     EntityManager manager;
      public PacienteDAO() {
           emf = Persistence.createEntityManagerFactory("aplicacion");
           manager=emf.createEntityManager();
    }
      
      //     
//     Guardar un usuario nuevo
     public Paciente agregarPaciente(Paciente paciente)  {
        emf = Persistence.createEntityManagerFactory("aplicacion");
        manager=emf.createEntityManager();
        manager.getTransaction().begin();
        try {
       manager.persist(paciente);
       manager.getTransaction().commit();
       
        } catch (Exception ex) {
            
            System.out.print("-----------------Excepcion----"+ex);
        }
         manager.clear();
        manager.close();
       return paciente;
}
//     Obtener Persona de acuerdo a Usuario
     public Paciente obtenerUsuario(Integer idUsuario){
         emf = Persistence.createEntityManagerFactory("aplicacion");
        manager=emf.createEntityManager();
        Query query = manager.createQuery("SELECT p FROM Paciente p where p.idUsuario.idUsuario=:id");
        query.setParameter("id", idUsuario);
        Paciente paciente=new Paciente();
        System.out.print("Resultado:"+query.getResultList().size());
        for(int i=0;i<query.getResultList().size();i++){
            
            paciente=(Paciente) query.getResultList().get(i);
            System.out.print("iterando el medico:"+paciente);
        }
       return paciente;
        
    }
}
