/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.dao;

import com.igf.consultasmedicas.entity.Institucion;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Frank
 */
public class InstitucionDAO {
     EntityManagerFactory emf;
     EntityManager manager;
     
     public InstitucionDAO() {
           emf = Persistence.createEntityManagerFactory("aplicacion");
           manager=emf.createEntityManager();
    }
     
     public Institucion agregarInstitucion(Institucion institucion)  {
         emf = Persistence.createEntityManagerFactory("aplicacion");
        manager=emf.createEntityManager();
        manager.getTransaction().begin();
        try {
       manager.persist(institucion);
       manager.getTransaction().commit();
        } catch (Exception ex) {
            
            System.out.print("-----------------Excepcion----"+ex);
        }
         manager.clear();
        manager.close();
       return institucion;
}
//  Obtiene la institucion    
    public Institucion obtenerInstitucion(){
         emf = Persistence.createEntityManagerFactory("aplicacion");
        manager=emf.createEntityManager();
       Institucion institucion=new Institucion();
        institucion=manager.find(Institucion.class, 1);
        manager.refresh(institucion);
         manager.clear();
        manager.close();
        return institucion;
    }
//    Actualiza la institucion
    public Institucion actualizarInstitucion(Institucion institucion){
        manager=emf.createEntityManager();
        manager.getTransaction().begin();
        try{
            manager.merge(institucion);
            manager.getTransaction().commit();
            manager.refresh(institucion);
            manager.clear();
            manager.close();
        } catch (Exception ex) {
            
            System.out.print("-----------------Excepcion----"+ex);
        }
        return institucion;
    }
    
}
