/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.dao;

import com.igf.consultasmedicas.entity.Especialidad;
import com.igf.consultasmedicas.entity.Medico;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Frank
 */

public class MedicoDAO {

    EntityManagerFactory emf;
     EntityManager manager;
    
     public MedicoDAO() {
       emf = Persistence.createEntityManagerFactory("aplicacion");
       manager=emf.createEntityManager();
    }
//  Guardar medico y su respectivo usuario    
    public Medico agregarMedico(Medico medico)  {
       emf = Persistence.createEntityManagerFactory("aplicacion");
       manager=emf.createEntityManager();
       manager.getTransaction().begin();
       try {
       manager.persist(medico);
       manager.getTransaction().commit();
       manager.refresh(medico);
        } catch (Exception ex) {
            
            System.out.print("-----------------Excepcion----"+ex);
        }
       manager.clear();
        manager.close();
       return medico;
}
//
//  Lista los medicos    
    public List<Medico> listarMedicos(){
        emf = Persistence.createEntityManagerFactory("aplicacion");
        manager=emf.createEntityManager();
        Query query = manager.createQuery("SELECT m FROM Medico m where m.estadoMedico='true'");
        List<Medico>medicos=(List<Medico>)query.getResultList();
       
        System.out.print("Hay actualmante --"+medicos.size()+"--------------- Medicos: -------------");
        manager.clear();
        manager.close();
         return medicos;
    }
//    
//  Lista las especialidades
    public List<Especialidad> listarEspecialidades(){
        manager=emf.createEntityManager();
        Query query=manager.createQuery("SELECT e FROm Especialidad e");
        List<Especialidad>especialidades=(List<Especialidad>)query.getResultList();
        System.out.print("Hay actualmante --"+especialidades.size()+"--------------- Especialidades: -------------");
        manager.clear();
        manager.close();
        return especialidades;
    }
    
//    Obtener medico por id
    public Medico obtenerMedico(Integer id){
         manager=emf.createEntityManager();
         Medico medico=new Medico();
         medico=manager.find(Medico.class, id);
         manager.clear();
         manager.close();
         return medico;
    }
    
//  Dar de baja el Doctor
    public void actualizarMedico(Medico medico){
        manager=emf.createEntityManager();
        manager.getTransaction().begin();
         try{
            manager.merge(medico);
            manager.getTransaction().commit();
            manager.clear();
            manager.close();
        } catch (Exception ex) {
            
            System.out.print("-----------------Excepcion----"+ex);
        }
    }
//  Obtener Doctor de acuerdo a el usuario
    public Medico obtenerUsuario(Integer idUsuario){
        emf = Persistence.createEntityManagerFactory("aplicacion");
        manager=emf.createEntityManager();
        Query query = manager.createQuery("SELECT m FROM Medico m where m.idUsuario.idUsuario=:id");
        query.setParameter("id", idUsuario);
        Medico medico=new Medico();
        System.out.print("Resultado:"+query.getResultList().size());
        for(int i=0;i<query.getResultList().size();i++){
            
            medico=(Medico) query.getResultList().get(i);
            System.out.print("iterando el medico:"+medico);
        }
       return medico;
        
    }
    
}
