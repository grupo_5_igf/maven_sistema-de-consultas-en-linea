/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.dao;

import com.igf.consultasmedicas.entity.Horario;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author mata
 */
public class HorarioDAO {
     EntityManagerFactory emf;
     EntityManager manager;
     
     public HorarioDAO() {
           emf = Persistence.createEntityManagerFactory("aplicacion");
           manager=emf.createEntityManager();
    }
    //    Actualiza el horario
    public Horario actualizarHorario(Horario horario){
        
         manager=emf.createEntityManager();
        manager.getTransaction().begin();
        try{
            manager.merge(horario);
           
            manager.getTransaction().commit();
            manager.refresh(horario);
            manager.clear();
            manager.close();
        } catch (Exception ex) {
            
            System.out.print("-----------------Excepcion----"+ex);
        }
        return horario;
    }
    
    //    Obtener horario por id
    public Horario obtenerHorario(Integer id){
         manager=emf.createEntityManager();
         Horario horario=new Horario();
         horario=manager.find(Horario.class, id);
         manager.refresh(horario);
         manager.clear();
         manager.close();
         return horario;
    }
}
