/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.igf.consultasmedicas.beans;


import com.igf.consultasmedicas.dao.MedicoDAO;
import com.igf.consultasmedicas.dao.PacienteDAO;
import com.igf.consultasmedicas.dao.UsuarioDAO;
import com.igf.consultasmedicas.entity.Expediente;
import com.igf.consultasmedicas.entity.Medico;
import com.igf.consultasmedicas.entity.Paciente;
import com.igf.consultasmedicas.entity.Usuario;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import com.igf.consultasmedicas.security.Encoders;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;

/**
 *
 * @author mata
 */
@SessionScoped
@ManagedBean
public class BeanLogin implements Serializable {
    
    private String nombreUsuario;
     private String pag;
     private UsuarioDAO usuarioDAO;
     private Medico medico=new Medico();
 private Paciente paciente=new Paciente();
     private Usuario usuario=new Usuario();
     private String nombre;
     private String apellido;
     private String telefono;
     private String direccion;
     private String sexo;
     private String estadoC;
     private String correo;

    /**
     * Creates a new instance of Saludo
     */
    public BeanLogin() {
    }
    
     @PostConstruct
     public void init(){
          System.out.print("Verificando usuario....");
         if(this.usuario.getIdUsuario()==null){
             System.out.print("cuidado");
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"error","Necesita autenticarse para proseguir"));
             
         }
     }
     
     @PreDestroy
    public void sessionDestroyed() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ha finalizado sesion."));
    }
    
    public String buttonAction(String nombreUsuario, String password) {
          System.out.print("Se esta intentando autenticar alguien con el nombre : "+nombreUsuario);
            password=Encoders.encrypt(password);
             usuarioDAO=new UsuarioDAO();
             Usuario login=usuarioDAO.obtenerUsuario(nombreUsuario);
             setUsuario(login);
             if(login.getContrasenia() == null ? password == null : login.getContrasenia().equals(password)){ 
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", "Un placer atenderle."));
             System.out.print("El usuario con el nombre:"+nombreUsuario+" ha ingresado al sistema");
             obtenerPersona(getUsuario());
             this.setUsuarioLogeado(nombreUsuario);
              return "/menu?faces-redirect=true";
             }else{
                  FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error!", "Credenciales incorrectas"));
             return "/index.xhtml";
             }
    }
      
      public String registrar(String nombreUsuario,String password,String passwordRepeat){
          if(password == null ? passwordRepeat != null : !password.equals(passwordRepeat)){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"error","Ambas contraseñas deben coincidir"));
          return "/nuevoUsuario.xhtml";
          }
          usuarioDAO=new UsuarioDAO();
          if(usuarioDAO.verificarNombre(nombreUsuario)==false){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"error","La persona con el nombre de usuario: "+nombreUsuario+" ya existe." ));
          return "/nuevoUsuario.xhtml";
          }
          usuario=new Usuario();
          usuario.setContrasenia(Encoders.encrypt(password));
          usuario.setEstadoUsuario(Boolean.TRUE);
          usuario.setNombreUsuario(nombreUsuario);
          System.out.print(usuario.getNombreUsuario());
          usuario.setRol(3);
          usuarioDAO.agregarUsuario(usuario);
          FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Persona registrada con exito ahora agregue su informacion personal"));
          return "/nuevoPaciente.xhtml";
      }
      
      public String guardarPersona(){
          PacienteDAO pacienteDAO=new PacienteDAO();
          System.out.print(getApellido());
          System.out.print(getDireccion());
          System.out.print(getTelefono());
          paciente=new Paciente();
          paciente.setUsuario(usuario.getIdUsuario());
          paciente.setApellidoPaciente(apellido);
          paciente.setCorreoPaciente(correo);
          paciente.setDireccionPaciente(direccion);
          paciente.setEstadoCivil(estadoC);
          paciente.setNombrePaciente(nombre);
          paciente.setSexo(sexo.charAt(0));
          paciente.setTelefonoPaciente(telefono);
          paciente.setEstadoPaciente(Boolean.TRUE);
          Expediente expediente=new Expediente();
          expediente.setFechaApertura(new Date());
          expediente.setNumeroExpediente(Integer.toString(Encoders.generateRamdomNumber(1, 99999)));
          paciente.setIdExpediente(expediente);
          pacienteDAO.agregarPaciente(paciente);
          FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Persona registrada con exito."));
          return "/index.xhtml";
      }
    
     public String outLogin() {
         this.nombreUsuario=null;
        
         this.usuario=null;
         this.usuario=new Usuario();
        sessionDestroyed();
         return "/index?faces-redirect=true";
    }
     
//     Obtener Usuario por Doctor o Paciente
     public void obtenerPersona(Usuario usuario){
         if(usuario.getIdRol().getIdRol()==1){
             MedicoDAO medicoDAO=new MedicoDAO();
             setMedico(medicoDAO.obtenerUsuario(usuario.getIdUsuario()));
         }else if(usuario.getIdRol().getIdRol()==3){
             PacienteDAO pacienteDAO=new PacienteDAO();
             setPaciente(pacienteDAO.obtenerUsuario(usuario.getIdUsuario()));
         }
     }
     
    public String getUsuarioLogeado() {
        return nombreUsuario;
    }

    public void setUsuarioLogeado(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }
    
    public void setPag(String pag) {
        this.pag = pag;
    }

    public String getPag() {
        return this.pag;
    }
    
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEstadoC() {
        return estadoC;
    }

    public void setEstadoC(String estadoC) {
        this.estadoC = estadoC;
    }
    
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
     
    public Usuario getUsuario() {
        System.out.print(usuario.getRol());
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        System.out.print("Rol:"+usuario.getRol());
        this.usuario = usuario;
    }
    
    
    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
    
}
