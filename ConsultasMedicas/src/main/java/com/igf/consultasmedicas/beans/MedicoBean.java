/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.beans;

import com.igf.consultasmedicas.dao.MedicoDAO;
import com.igf.consultasmedicas.entity.Especialidad;
import com.igf.consultasmedicas.entity.Medico;
import com.igf.consultasmedicas.entity.Usuario;
import com.igf.consultasmedicas.security.Encoders;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;


/**
 *
 * @author Frank
 */
@ManagedBean(name="medicoBean")
@ViewScoped
public class MedicoBean implements Serializable {
    MedicoDAO medicoDAO;
    private Medico medico=new Medico();
    private Usuario usuario=new Usuario();
    private String repeatContrasenia;
    private List<Medico> medicos;
    private List<Especialidad>especialidades;
    private String prueba;
    
     /**
     * Creates a new instance of MedicoBean
     */
    public MedicoBean() {
    }
    
    @PostConstruct
    public void init(){
      medicoDAO=new MedicoDAO();
      
      System.out.print("Listo para traer Medicos");
       setMedicos(medicoDAO.listarMedicos());
    }
    
    public String agregarMedico(String especialidad) throws Exception{
        if(usuario.getContrasenia() == null ? repeatContrasenia != null : !usuario.getContrasenia().equals(repeatContrasenia)){
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"error","Ambas contraseñas deben coincidir"));
          return "/nuevoMedico.xhtml";
        }
        System.out.print(especialidad);
        usuario.setRol(1);
        usuario.setEstadoUsuario(Boolean.TRUE);
        usuario.setContrasenia(Encoders.encrypt(repeatContrasenia));
        medico.setIdUsuario(usuario);
        medico.setEspecialidad(Integer.parseInt(especialidad));
        medico.setInstitucion(1);
        medico.setEstadoMedico(Boolean.TRUE);
        medicoDAO.agregarMedico(medico);
        init();
       return "/medicos?faces-redirect=true";
    }
    
     public String obtenerMedico(){
        System.out.print("Mostrar el detalle de medico:"+ medico.toString());
        medicoDAO=new MedicoDAO();
        setMedico(medicoDAO.obtenerMedico(medico.getIdMedico()));
        return "/medico?faces-redirect=false";
    }
     public void darDeBaja(Medico medico) throws IOException{
         medicoDAO=new MedicoDAO();
         medico.setEstadoMedico(Boolean.FALSE);
         medicoDAO.actualizarMedico(medico);
         init();
          ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
     }
     
    public String nuevo(){
            
            
            return "/nuevoMedico.xhtml";
    }
   
     public List<Medico> getMedicos() {
        
        return medicos;
    }
     
  
    public void setMedicos(List<Medico> medicos) {
        this.medicos = medicos;
    }
    
      public List<Especialidad> getEspecialidades() {
        return medicoDAO.listarEspecialidades();
    }
      
    public void setEspecialidades(List<Especialidad> especialidades) {
        this.especialidades = especialidades;
    }
    
    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }
    
    
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public String getRepeatContrasenia() {
        return repeatContrasenia;
    }

    public void setRepeatContrasenia(String repeatContrasenia) {
        this.repeatContrasenia = repeatContrasenia;
    }
    
    public String getPrueba() {
        return prueba;
    }

    public void setPrueba(String prueba) {
        this.prueba = prueba;
    }
    
}
