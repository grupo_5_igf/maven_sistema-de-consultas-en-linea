/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.beans;

import com.igf.consultasmedicas.dao.DiagnosticoDAO;
import com.igf.consultasmedicas.dao.ExpedienteDAO;
import com.igf.consultasmedicas.entity.Diagnosticos;
import com.igf.consultasmedicas.entity.Expediente;
import com.igf.consultasmedicas.entity.Paciente;
import java.io.Serializable;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;


/**
 *
 * @author mata
 */

@ManagedBean(name="consultaBean")
@SessionScoped
public class ConsultaBean implements Serializable  {
    private Expediente expediente=new Expediente();
    private Diagnosticos diagnostico=new Diagnosticos();
    private Paciente paciente = new Paciente();
    private Integer IdExpediente;
    
     public String terminarConsulta(){
    System.out.print("Prueba");
         FacesContext context = FacesContext.getCurrentInstance();
    Map<String,String> params = context.getExternalContext().getRequestParameterMap();
    String id = params.get("exp123");
         setIdExpediente(Integer.parseInt(id));
    ExpedienteDAO expedienteDAO=new ExpedienteDAO();
         setPaciente(expedienteDAO.obtenerPaciente(Integer.parseInt(id)));
    System.out.print("Expediente: "+id);
    return "/diagnostico?faces-redirect=true";
     }
     
    
    
    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        System.out.print(paciente);
        this.paciente = paciente;
    }

    public Integer getIdExpediente() {
        return IdExpediente;
    }

    public void setIdExpediente(Integer IdExpediente) {
        this.IdExpediente = IdExpediente;
    }
    
    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        System.out.print(expediente);
        this.expediente = expediente;
    }
    
    public Diagnosticos getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(Diagnosticos diagnostico) {
        this.diagnostico = diagnostico;
    }
    
   
    
}
