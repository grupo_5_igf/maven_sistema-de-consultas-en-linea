/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.beans;

import com.igf.consultasmedicas.dao.ExpedienteDAO;
import com.igf.consultasmedicas.entity.Paciente;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author mata
 */
@ManagedBean(name="expedienteBean")
@ViewScoped
public class ExpedienteBean {
    ExpedienteDAO expedienteDAO;
    private Paciente paciente=new Paciente();
    private List<Paciente>pacientes;
    /**
     * Creates a new instance of MedicoBean
     */
    public ExpedienteBean() {
    }
    
    @PostConstruct
    public void init(){
      expedienteDAO=new ExpedienteDAO();
      System.out.print("Listo para traer Medicos");
       setPacientes(expedienteDAO.listarPacientes());
    }
    
    public String obtenerExpediente(){
         System.out.print("Mostrar el detalle de el expediente:"+ paciente.toString());
         expedienteDAO=new ExpedienteDAO();
         setPaciente(expedienteDAO.obtenerPaciente(paciente.getIdPersona()));
         return "/expediente?faces-redirect=false";
    }
    
    public void darDeBaja(Paciente paciente) throws IOException{
        expedienteDAO=new ExpedienteDAO();
        paciente.setEstadoPaciente(Boolean.FALSE);
        expedienteDAO.actualizarPaciente(paciente);
        init();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }
    
     public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public List<Paciente> getPacientes() {
        return pacientes;
    }

    public void setPacientes(List<Paciente> pacientes) {
        this.pacientes = pacientes;
    }
}
