/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.beans;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author mata
 */
@ApplicationScoped
@ManagedBean(name = "param")
public class BeanTabMenu {
    public int pag=1;

    public void setPag(int pag) {
        this.pag = pag;
    }

    public int getPag() {
        return 1;
    }
    
}
