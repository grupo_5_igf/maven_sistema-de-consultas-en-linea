/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.beans;

import com.igf.consultasmedicas.dao.HorarioDAO;
import com.igf.consultasmedicas.dao.InstitucionDAO;
import com.igf.consultasmedicas.entity.Horario;
import com.igf.consultasmedicas.entity.Institucion;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Frank
 */
@Named(value = "institucionBean")
@SessionScoped
public class InstitucionBean implements Serializable {
    
    private int idInstitucion;
     private String nombreInstitucion;
     private String direccionInstitucion;
     private String correoInstitucion;
     private String telefonoInstitucion;
     private Institucion institucion;

     private String horaInicio;
     private String horaFinal;

    
       
    /**
     * Creates a new instance of InstitucionBean
     */
    public InstitucionBean() {
    }
     
     public void addInstitucion(){
        InstitucionDAO institucionDAO = new InstitucionDAO();
        institucionDAO.agregarInstitucion(institucion);
     }
     
    
      @PostConstruct
     public void returnInstitucion(){
        InstitucionDAO instDAO = new InstitucionDAO();
        Institucion inst = instDAO.obtenerInstitucion();
        if (inst != null){
            
            setInstitucion(inst);
            setIdInstitucion(inst.getIdInstitucion());
            setNombreInstitucion(inst.getNombreInstitucion());
            setDireccionInstitucion(inst.getDireccionInstitucion());
            setCorreoInstitucion(inst.getCorreoInstitucion());
            setTelefonoInstitucion(inst.getTelefonoInstitucion());
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Institucion no Encontrada"));
        }
       
     }
     
      public String updateInstitucion(){

         institucion.setCorreoInstitucion(correoInstitucion);
         institucion.setDireccionInstitucion(direccionInstitucion);
         institucion.setNombreInstitucion(nombreInstitucion);
         institucion.setTelefonoInstitucion(telefonoInstitucion);
         for(Horario horario: institucion.getHorarioList()){
         System.out.print("modificado:"+horario.getHoraInicio()); 
      }
         
         InstitucionDAO instDAO = new InstitucionDAO();
          setInstitucion(instDAO.actualizarInstitucion(institucion));
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Institucion Actualizada"));
         return "/institucion.xhtml";
     }
      
      public String modificar(Institucion institucion){
           System.out.print("Mostrar el detalle de la institucion:"+ institucion.getHorarioList().size());
           InstitucionDAO instDAO = new InstitucionDAO();
       
          setInstitucion(instDAO.obtenerInstitucion());
        return "/institucionEditar?faces-redirect=false";
      }
     
      public void modificarHorario(){
          System.out.print("hola"+horaInicio);
      }
     
    /** 
     * @return the idInstitucion
     */
    public int getIdInstitucion() {
        return idInstitucion;
    }

    /**
     * @param idInstitucion the idInstitucion to set
     */
    public void setIdInstitucion(int idInstitucion) {
        this.idInstitucion = idInstitucion;
    }

    /**
     * @return the nombreInstitucion
     */
    public String getNombreInstitucion() {
        return nombreInstitucion;
    }

    /**
     * @param nombreInstitucion the nombreInstitucion to set
     */
    public void setNombreInstitucion(String nombreInstitucion) {
        this.nombreInstitucion = nombreInstitucion;
    }

    /**
     * @return the direccionInstitucion
     */
    public String getDireccionInstitucion() {
        return direccionInstitucion;
    }

    /**
     * @param direccionInstitucion the direccionInstitucion to set
     */
    public void setDireccionInstitucion(String direccionInstitucion) {
        this.direccionInstitucion = direccionInstitucion;
    }

    /**
     * @return the correoInstitucion
     */
    public String getCorreoInstitucion() {
        return correoInstitucion;
    }

    /**
     * @param correoInstitucion the correoInstitucion to set
     */
    public void setCorreoInstitucion(String correoInstitucion) {
        this.correoInstitucion = correoInstitucion;
    }

    /**
     * @return the telefonoInstitucion
     */
    public String getTelefonoInstitucion() {
        return telefonoInstitucion;
    }

    /**
     * @param telefonoInstitucion the telefonoInstitucion to set
     */
    public void setTelefonoInstitucion(String telefonoInstitucion) {
        this.telefonoInstitucion = telefonoInstitucion;
    }
    
    
    public Institucion getInstitucion() {
        return institucion;
    }

    public void setInstitucion(Institucion institucion) {
        this.institucion = institucion;
    }
    
    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(String horaFinal) {
        this.horaFinal = horaFinal;
    }
     
     
   
    
}
