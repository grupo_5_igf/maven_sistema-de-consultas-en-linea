/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.beans;

import com.igf.consultasmedicas.dao.DiagnosticoDAO;
import com.igf.consultasmedicas.dao.ExpedienteDAO;
import com.igf.consultasmedicas.dao.ResultadoDAO;
import com.igf.consultasmedicas.dao.TratamientoDAO;
import com.igf.consultasmedicas.entity.Diagnosticos;
import com.igf.consultasmedicas.entity.Examen;
import com.igf.consultasmedicas.entity.Expediente;
import com.igf.consultasmedicas.entity.Paciente;
import com.igf.consultasmedicas.entity.Resultado;
import com.igf.consultasmedicas.entity.Tratamiento;
import com.igf.consultasmedicas.security.Encoders;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author mata
 */
@ManagedBean(name="tratamientoBean")
@SessionScoped
public class TratamientoBean implements Serializable{
    TratamientoDAO tratamientoDAO;
    private Expediente expediente=new Expediente();
    private Tratamiento tratamiento=new Tratamiento();
    private List<Tratamiento> tratamientos;
    private Paciente paciente = new Paciente();
    private List<Diagnosticos>diagnosticos=new ArrayList<Diagnosticos>();
    private List<Resultado>resultados=new ArrayList<Resultado>();
    private List<String> images;
    private Resultado resultado=new Resultado();
    private StreamedContent listImage = null;
    private String lectura;

    public String getLectura() {
        return lectura;
    }

    public void setLectura(String lectura) {
        this.lectura = lectura;
    }
   
     @PostConstruct
    public void init() {
        images = new ArrayList<String>();
       for(int i = 1; i <= 3; i++){
        images.add("./resources/imagenes/examenes/examen"+i+"jpg");
    }
    }
    
     public String terminarConsulta(){
    System.out.print("Prueba");
         FacesContext context = FacesContext.getCurrentInstance();
    Map<String,String> params = context.getExternalContext().getRequestParameterMap();
    String id = params.get("exp123");
        
    ExpedienteDAO expedienteDAO=new ExpedienteDAO();
         setPaciente(expedienteDAO.obtenerPaciente(Integer.parseInt(id)));
    System.out.print("Expediente: "+id);
    return "/nuevoTratamiento?faces-redirect=true";
     }
    
     public String obtenerTratamiento(){
        System.out.print("Mostrar el detalle de medico:"+ tratamiento.toString());
        tratamientoDAO=new TratamientoDAO();
        setTratamiento(tratamientoDAO.obtenerTratamiento(tratamiento.getIdTratamiento()));
        return "/medico?faces-redirect=false";
    }
     
     public String guardarTratamiento(){
         HttpServletRequest request = null;
         FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        String file = params.get("examen");
         
         tratamiento.setIdExpediente(paciente.getIdExpediente());
         TratamientoDAO tratamientoDAO=new TratamientoDAO();
         tratamiento=tratamientoDAO.agregarTratamiento(tratamiento);
         if(this.diagnosticos.size()>0){
             DiagnosticoDAO diagnosticosDAO=new DiagnosticoDAO();
             for(Diagnosticos diagnostico: diagnosticos){
                 diagnostico.setIdTratamiento(tratamiento);
                 diagnosticosDAO.agregarDiagnostico(diagnostico);
             }
         }
         if(!file.isEmpty()){
             ResultadoDAO resultadoDAO=new ResultadoDAO();
                 resultado.setIdTratamiento(tratamiento);
                 resultado.setLectura(lectura);
                 Examen examen=new Examen();
                 examen.setNombreExamen(guardarImagen(file,request));
                 resultado.setIdExamen(examen);
                 
                 resultadoDAO.agregarDiagnostico(resultado);
         }
         this.diagnosticos=new ArrayList<Diagnosticos>();
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Guardado", "La connsulta ha sido almacenada."));
         return "/expedientes?faces-redirect=true";
     }
     
     public String guardarImagen(String imagen,HttpServletRequest request){
        ServletContext servletContext = (ServletContext) FacesContext
    .getCurrentInstance().getExternalContext().getContext();
        String value = servletContext.getRealPath("/");
        
        String nombre = null;
         String ruta = null;
         System.out.print(imagen);
         try{
            if(imagen!=null){
             String imageDataBytes = imagen.substring(imagen.indexOf(",")+1);
             
            Base64.Decoder deco = Base64.getDecoder();
            //Decodifica el String de base 64 a un array de bytes
            byte[] decodedBytes =deco.decode(imageDataBytes);
            InputStream stream = new ByteArrayInputStream(decodedBytes);
            BufferedImage image = ImageIO.read(stream);
            stream.close();
            nombre=String.valueOf(Encoders.generateRamdomNumber(1, 1000));
            Date date = new Date();
            DateFormat df = new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss");
            String fecha=df.format(date);
            System.out.print(fecha);
            nombre=Encoders.encrypt(nombre+fecha);
            nombre="\\"+nombre+".png";
            System.out.print("Esta es prueba");
            ruta= value+"..\\..\\src\\main\\webapp\\resources\\imagenes\\examenes"+nombre;
            System.out.print(value+nombre);
            
            System.out.print("ruta:: "+ruta);
            //Direccion donde se guardara el archivo, Pero luego se guardara en un almacen en la nube
            File outputfile = new File(ruta);
            //Guardado de la los demas atributos
            System.out.println("Archivo:"+ outputfile);
          
            //////////////////////////////////////
            ImageIO.write(image, "png", outputfile);
            }
            
       } catch (IOException ex) {
           System.out.println(ex.getMessage());
         }
         return nombre;
     }
     
    
      public String guardarDiagnosticos(Tratamiento tratamiento){
         if(this.diagnosticos.size()>0){
             DiagnosticoDAO diagnosticosDAO=new DiagnosticoDAO();
             for(Diagnosticos diagnostico: diagnosticos){
                 diagnostico.setIdTratamiento(tratamiento);
                 diagnosticosDAO.agregarDiagnostico(diagnostico);
             }
         }
         this.diagnosticos=new ArrayList<Diagnosticos>();
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Guardado", "La connsulta ha sido almacenada."));
         return "/expedientes?faces-redirect=true";
     }
     public List<Tratamiento> listarTratamientos(){
         tratamientoDAO=new TratamientoDAO();
         setTratamientos(tratamientoDAO.listarTratamientos());
         return getTratamientos();
     }
    public void agregarSintoma(){
        System.out.print("Agregando nuevo diagnostivo:"+this.diagnosticos.size());
        this.diagnosticos.add(new Diagnosticos());
    }
    
    public void agregarExamen(){
        System.out.print("Agregando nuevo examen:"+this.resultados.size());
        this.resultados.add(new Resultado());
    }
    
    public void quitarExamen(){
        this.resultados.remove(resultados.size()-1);
    }
    
    public List<String> obtenerExamenes(){
    List<String>examenes=new ArrayList<String>();
    
    return examenes;
}
     
    public Tratamiento getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(Tratamiento tratamiento) {
        this.tratamiento = tratamiento;
    }

    public List<Tratamiento> getTratamientos() {
        return tratamientos;
    }

    public void setTratamientos(List<Tratamiento> tratamientos) {
        this.tratamientos = tratamientos;
    }
    

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }
    
    
    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
    
    public List<Diagnosticos> getDiagnosticos() {
        return diagnosticos;
    }
    
   
    public void setDiagnosticos(List<Diagnosticos> diagnosticos) {
        this.diagnosticos = diagnosticos;
    }
    
    
    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
    
    public StreamedContent getListImage() {
        return listImage;
    }

    public void setListImage(StreamedContent listImage) {
        this.listImage = listImage;
    }
    
    

    
    public List<Resultado> getResultados() {
        return resultados;
    }

    public void setResultados(List<Resultado> resultados) {
        this.resultados = resultados;
    }
    
    
    public Resultado getResultado() {
        return resultado;
    }

    public void setResultado(Resultado resultado) {
        this.resultado = resultado;
    }
}
