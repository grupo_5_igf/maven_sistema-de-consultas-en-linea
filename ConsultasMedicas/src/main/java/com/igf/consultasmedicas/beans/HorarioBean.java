/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igf.consultasmedicas.beans;

import com.igf.consultasmedicas.dao.HorarioDAO;
import com.igf.consultasmedicas.entity.Horario;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author mata
 */
@ManagedBean(name="horarioBean")
@ViewScoped
public class HorarioBean {
     HorarioDAO horarioDAO;
    private int idHorario;
    private Horario horario;
    private String horaInicio;
    SimpleDateFormat formatter = new SimpleDateFormat("h:mm");
   
    private String horaFinal;
    

     public String obtenerHorario(){
        System.out.print("Mostrar el detalle de Horario:"+ idHorario);
        horarioDAO=new HorarioDAO();
        setHorario(horarioDAO.obtenerHorario(idHorario));
        return "/medico?faces-redirect=false";
    }
     
      public String updateHorario() throws ParseException{
          System.out.print("hora Inicio"+horaInicio);
         horarioDAO = new HorarioDAO();
         horario.setHoraInicio(formatter.parse(horaInicio));
         horario.setHoraFinal(formatter.parse(horaFinal));
         horarioDAO.actualizarHorario(horario);
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Institucion Actualizada"));
         return "/institucion.xhtml";
     }
    
    public int getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(int idHorario) {
        this.idHorario = idHorario;
    }

    public Horario getHorario() {
        return horario;
    }

    public void setHorario(Horario horario) {
        this.horario = horario;
    }
     public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(String horaFinal) {
        this.horaFinal = horaFinal;
    }
}
