 var canal;
 var expediente=$('.expediente');
 var idExpediente=$('#expediente');
 var contenido=$('.contenido'); 
 // Apreton de manos
 var user=$('#user').val();
 var rol=$('#rol').val();
 var especialidad=$('#especialidad').val();
 var ws = new WebSocket("ws://localhost:3000/");
  
 ws.onopen = function() {
     ws.send(JSON.stringify({
         nombre: user,
         rol: rol,
         presentacion: true,
         estado: false,
         especialidad: especialidad,
         accion: "saludo"
     }));
 };
 
//Escuchador de entrada de mensajes
 ws.onmessage = function (evt) {
    console.log(JSON.parse(evt.data));
    data=JSON.parse(evt.data);
    canal=data.canal;
    idExpediente.val(data.idExpediente);
      switch (data.accion){
          case "mensaje":
               contenido.append($("<p class='"+data.color+"'>"+data.rol+" "+data.nombre+":  "+data.mensaje+"</p><br>"));
          break;
            case "obtenerDoctores" :
               mostrarDoctores(data);
          break;
          case "agregarDoctor":
                deshabilitarAgregarMedico();
                break;
            case "confirmarDoctor":
                rol=data.rol;
                break;
      }
      if(data.rol=="Ayudante" && data.accion=="agregarDoctor"){
          rol="Ayudante";
      }
         if(data.presentacion){
   swal('Nuevo Paciente requiere de sus servicios');
   $("#obtMedicos").removeClass("disabled");
             }
     
      if(data.imagen!=null){
        mostrarImagen(data.imagen);
    }
     
 };
//    Escuchador de eventos para enviar mensajes
    $('#butt').click( function() {
       console.log("tu canal es:"+canal);
        console.log(user);
        var input=$('.inputMessage').val();
        $('.inputMessage').val("");
        if(input==''){
            return 0;
            }
                    //Envio de mensaje
        ws.send(JSON.stringify({        
            nombre: user,
            mensaje: input,
            rol: rol,
            estado: false,
            presentacion: false,
            canal: canal,
            accion: "mensaje"
            }));
       //Escuchador de entrada de mensajes
   
   
        ws.onclose = function() {
        console.log("coneccion ha sido cerrada");
         };
        ws.onerror = function(err) {
        console.log("Se ha producido un error ...")
        
        };
    });
//    Mostrar Imagen
    mostrarImagen=function(imagen){
         contenido.append($("<img class='materialboxed' width='65%' src='"+imagen+"' ></img><a href='"+imagen+"' download='imagen.jpg'>Descargar</a>"));
           $('.materialboxed').materialbox();
    };
//    Deshabilitar Agregar medicos
    deshabilitarAgregarMedico=function(){
        console.log("Deshabilitando boton");
        $("#obtMedicos").addClass("disabled");
    };
//    MOstrar los doctores ayudantes disponibles
    mostrarDoctores=function(data){
        if(data.medicos.length==0){
            Materialize.toast('No hay mas medicos disponibles por el momento', 4000);
            return 0;
        }
     $(document).ready(function(){
            $('#modalIng').openModal();
    });
         var medicos=$('#doctores');
         medicos.html("");
        data.medicos.forEach(function(medico){
              medicos.append("<p><input name='group1' type='radio' id='"+medico.id+"' value='"+medico.id+"' /><label for='"+medico.id+"'>"+medico.nombre+"-"+medico.especialidad+"</label></p>");
         }); 
    };
//    Agregar al doctor elegido a la conversación
    $('#agregarMedico').click( function() {
        var elegido = $("input:radio[name ='group1']:checked").val();
         $('#modalIng').closeModal();
        console.log("Elegido es: "+elegido);
         //Envio de mensaje
        ws.send(JSON.stringify({        
            nombre: user,
            mensaje: "Agregado de Nuevo Doctor",
            rol: rol,
            estado: false,
            presentacion: false,
            canal: canal,
            nuevoMedico: parseInt(elegido),
            accion: "agregarDoctor"
            }));
    });
    
//    Obtener lista de medicos
    $('#obtMedicos').click( function() {
         //Envio de mensaje
        ws.send(JSON.stringify({        
            nombre: user,
            canal: canal,
            accion: "obtenerDoctores"
            }));
    });
//    Habilitar o deshabilitar medico
    $("#switch").click(function(){
       var seleccion=$("#switch").prop('checked');
        console.log(seleccion);
         //Envio de mensaje
        ws.send(JSON.stringify({        
            nombre: user,
            estado: seleccion,
            accion: "cambiarEstado"
            }));
    });
//  Cancelar la consulta por parte del Medico
    $("#cancelar").click(function(){
        console.log(rol);
       console.log("cancelando");
        ws.send(JSON.stringify({        
            canal: canal,
            rol: rol,
            accion: "cancelar"
            }));
    });
    
function start() {
    PF('statusDialog').show();
}
 
function stop() {
    PF('statusDialog').hide();
}
    
    