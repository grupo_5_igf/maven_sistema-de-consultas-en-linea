 var accion="mensaje";
 var canal;
 var contenido=$('.contenido'); 
 // Apreton de manos
 var user=$('#user').val();
 var rol=$('#rol').val();
 var idExpediente=$('#idExpediente').val();
 var ws=WebSocket;
 var color;
 var imagen=null;
 
 //Escuchador de entrada de mensajes
 ws.onmessage = function (evt) {
    console.log(JSON.parse(evt.data));
    data=JSON.parse(evt.data);
    canal=data.canal;
    
     switch (data.accion){
          case "mensaje":
               contenido.append($("<p class='"+data.color+"'>"+data.rol+" "+data.nombre+":  "+data.mensaje+"</p><br>"));
          break;
           case "agregarPaciente" :
               mostrarDoctores(data);
          break;
            case "agregarDoctor":
                agregarAyudante(data);
            break;
     }

    if(data.imagen!=null){
        mostrarImagen(data.imagen);
    }
    };

//    Escuchador de eventos para enviar mensajes
    $('#butt').click( function() {
         console.log("tu canal es:"+canal);
        var input=$('.inputMessage').val();
        $('.inputMessage').val("");
        if(input=='' && imagen==null){
            return 0;
            }
        switch (accion){
            case "mensaje":
                console.log("caso mensaje");
                ws.send(JSON.stringify({        
                nombre: user,
                mensaje: input,
                rol: rol,
                presentacion: false,
                canal: canal,
                idExpediente: idExpediente,
                accion: accion 
                }));
            break;
            case "imagen":
                console.log("caso imagen");
                ws.send(JSON.stringify({        
                nombre: user,
                mensaje: input,
                rol: rol,
                presentacion: false,
                canal: canal,
                idExpediente: idExpediente,
                accion: accion, 
                imagen: imagen
                }));
                $('#image').attr('src', ' ');
                accion="mensaje";
            break;  
        
            
        
        }
      
                    //Envio de mensaje
       
           
       //Escuchador de entrada de mensajes
    ws.onmessage = function (evt) {
        console.log(JSON.parse(evt.data));
        data=JSON.parse(evt.data);
        canal=data.canal;
    switch (data.accion){
          case "agregarDoctorPrimario":
              $("#initChat").addClass("disabled");
               contenido.append($("<p class='"+data.color+"'>"+data.rol+" "+data.nombre+":  "+data.mensaje+"</p><br>"));
           case "mensaje":
               contenido.append($("<p class='"+data.color+"'>"+data.rol+" "+data.nombre+":  "+data.mensaje+"</p><br>"));
                break;
           case "agregarDoctor":
                agregarAyudante(data);
                break;
      }         if(data.imagen!=null){
        mostrarImagen(data.imagen);
    }
        };
        ws.onclose = function() {
        console.log("coneccion ha sido cerrada");
        var $toastContent = $('<span>La conexion ha sido cerrada</span>').add($('<button class="btn-flat toast-action">Undo</button>'));
            Materialize.toast($toastContent, 10000);
        contenido.append($("<p>"+"Consulta Finalizada"+"</p><br>"));    
         };
        ws.onerror = function(err) {
        console.log("Se ha producido un error ...")
        };
    });
    
    agregarImagen =function(){
      var preview = document.querySelector('#image');
       var file = document.querySelector('input[type=file]').files[0];
       $('input[type=file]').val('');
         var reader = new FileReader();
          reader.onloadend = function () {
                preview.src = reader.result; 
                 imagen=reader.result; 
                 accion="imagen";
            }
            if (file) {
                reader.readAsDataURL(file);
               
            } else {
                preview.src = "";
            }
    };
    
    //    MOstrar los doctores ayudantes disponibles
    mostrarDoctores=function(data){
        if(data.medicos.length==0){
            Materialize.toast('No hay mas medicos disponibles por el momento', 4000);
            return 0;
        }
        console.log("mira");
     $(document).ready(function(){
            $('#modalIng').openModal();
});
         var medicos=$('#doctores');
         medicos.html("");
         data.medicos.forEach(function(medico){
              medicos.append("<p><input name='group1' type='radio' id='"+medico.id+"' value='"+medico.id+"' /><label for='"+medico.id+"'>"+medico.nombre+"-"+medico.especialidad+"</label></p>");
         }); 
    };
    
    //    Agregar al doctor elegido a la conversación
    $('#agregarMedico').click( function() {
        var elegido = $("input:radio[name ='group1']:checked").val();
         $('#modalIng').closeModal();
        console.log("Elegido es: "+elegido);
         //Envio de mensaje
        ws.send(JSON.stringify({        
            nombre: user,
            mensaje: "Agregado de Nuevo Doctor",
            rol: rol,
            estado: false,
            presentacion: false,
            canal: canal,
            idExpediente: idExpediente,
            nuevoMedico: parseInt(elegido),
            accion: "agregarDoctorPrimario"
            }));
            //Escuchador de entrada de mensajes
   
    });
//  Opcion de Rechazar o denegar agregar Nuevo medico ayudante    
    agregarAyudante=function(data){
        swal({
  title: 'Se desea agregar un medico Auxiliar',
  text: "Necesitamos tu aprobación para que el ingreso del Doctor:"+data.medico.nombre+",Especialidad:"+data.medico.especialidad,
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si , Deseo que ingrese!'
},function(){
      enviarConfirmacion(data);
    swal(
      'Agregado a conversación!',
      'Espere a que se agrege a la conversación.',
      'success'
    );
    
  });

    };
//  Enviar confirmacion de aceptar doctor ayudante
    enviarConfirmacion=function(data){
        console.log(data);
         ws.send(JSON.stringify({        
            nombre: user,
            mensaje: "Agregado de Nuevo Doctor",
            rol: rol,
            estado: false,
            presentacion: false,
            canal: canal,
            nuevoMedico: data.nuevoMedico,
            accion: "confirmarDoctor"
            }));
    };
    
//  Mostrar Imagen
    mostrarImagen=function(imagen){
         contenido.append($("<img class='materialboxed' src='"+imagen+"' ></img>"));
         $('.materialboxed').materialbox();
    };
//  Iniciar el chat y buscar Doctores disponibles    
    $('#initChat').click( function() { 
        ws = new WebSocket("ws://localhost:3000/");
        ws.onopen = function() {
     ws.send(JSON.stringify({
        nombre: user,
        rol: rol,
        presentacion: true,
        idExpediente: idExpediente,
        accion: "agregarPaciente"
     }));
 };
 
 //Escuchador de entrada de mensajes
 ws.onmessage = function (evt) {
    console.log(JSON.parse(evt.data));
    data=JSON.parse(evt.data);
    canal=data.canal;
     switch (data.accion){
          case "mensaje":
               contenido.append($("<p class='"+data.color+"'>"+data.rol+" "+data.nombre+":  "+data.mensaje+"</p><br>"));
          break;
           case "agregarPaciente" :
               mostrarDoctores(data);
      }
    if(data.imagen!=null){
        mostrarImagen(data.imagen);
    }
    };
    });